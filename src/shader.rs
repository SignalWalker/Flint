use ash::{
    version::DeviceV1_0,
    vk::{PipelineShaderStageCreateInfo, ShaderModuleCreateInfo},
    Device,
};
use shaderc::*;

use std::collections::HashMap;
use std::ffi::CString;
use std::iter::FromIterator;

mod artifact;
mod descriptor;
mod pipeline;
mod stage;

pub use artifact::*;
pub use descriptor::*;
pub use pipeline::*;
pub use stage::*;

pub struct MetaShader {
    pub stage: ShaderStage,
    pub entry: String,
    pub bin: Vec<u32>,
}

impl MetaShader {
    pub fn new(
        compiler: &mut Compiler,
        path: &str,
        entry: &str,
        opt: Option<&CompileOptions>,
    ) -> Self {
        let stage: ShaderStage = path.split_at(path.rfind('.').unwrap() + 1).1.into();
        let bin = {
            let name = path.split_at(path.rfind('/').unwrap_or(0)).1;
            let src = std::fs::read_to_string(path).unwrap();
            match compiler.compile_into_spirv(&src, stage.into(), name, entry, opt) {
                Ok(b) => b,
                Err(e) => panic!("{}", e),
            }
            .as_binary()
            .to_vec()
        };
        Self {
            stage,
            entry: entry.to_string(),
            bin,
        }
    }

    pub fn new_chain(
        compiler: &mut Compiler,
        info: Vec<(&str, &str)>,
        opt: Option<&CompileOptions>,
    ) -> Vec<Self> {
        info.iter()
            .map(|(path, entry)| MetaShader::new(compiler, path, entry, opt))
            .collect()
    }

    fn build(self, dev: Device) -> ShaderArtifact {
        ShaderArtifact {
            module: unsafe {
                dev.create_shader_module(&ShaderModuleCreateInfo::builder().code(&self.bin), None)
            }
            .unwrap(),
            dev,
            stage: self.stage,
            entry: CString::new(self.entry).unwrap(),
        }
    }

    pub fn build_chain(
        dev: Device,
        meta: Vec<MetaShader>,
        min_offset: u64,
    ) -> (
        HashMap<ShaderStage, ShaderArtifact>,
        DescPoolToken,
        HashMap<String, PushConstant>,
    ) {
        let (pool, push_consts) = {
            let mut builder = DescPoolToken::builder(min_offset);
            for m in meta.iter() {
                builder.add(&m.bin);
            }
            builder.build(dev.clone())
        };
        (
            HashMap::from_iter(meta.into_iter().map(|m| (m.stage, m.build(dev.clone())))),
            pool,
            push_consts,
        )
    }
}
