use crate::base::surface::*;

use crate::shader::PushConstant;
use ash::extensions::khr::Swapchain;

use crate::command::*;
use crate::renderpass::RenderPassToken;
use crate::shader::DescPoolToken;
use crate::shader::PipeToken;
use crate::shader::ShaderArtifact;
use crate::shader::ShaderStage;
use crate::*;
use ash::version::DeviceV1_0;
use ash::{vk, Device, Instance};
use std::collections::HashMap;
use std::default::Default;

use std::ops::Drop;

fn query_support(
    physical_device: vk::PhysicalDevice,
    surface: &SurfToken,
) -> (
    vk::SurfaceCapabilitiesKHR,
    Vec<vk::SurfaceFormatKHR>,
    Vec<vk::PresentModeKHR>,
) {
    unsafe {
        let capabilities = surface
            .loader
            .get_physical_device_surface_capabilities(physical_device, surface.surface)
            .unwrap();
        let formats = surface
            .loader
            .get_physical_device_surface_formats(physical_device, surface.surface)
            .unwrap();
        let present_modes = surface
            .loader
            .get_physical_device_surface_present_modes(physical_device, surface.surface)
            .unwrap();

        (capabilities, formats, present_modes)
    }
}

fn choose_format(available: &[vk::SurfaceFormatKHR]) -> vk::SurfaceFormatKHR {
    available
        .iter()
        .map(|sfmt| match sfmt.format {
            vk::Format::UNDEFINED => vk::SurfaceFormatKHR {
                format: vk::Format::B8G8R8_UNORM,
                color_space: sfmt.color_space,
            },
            _ => *sfmt,
        })
        .nth(0)
        .unwrap()
    // if available.len() == 1 && available[0].format == vk::Format::UNDEFINED {
    //     return vk::SurfaceFormatKHR {
    //         format: vk::Format::B8G8R8A8_UNORM,
    //         color_space: vk::ColorSpaceKHR::SRGB_NONLINEAR,
    //     };
    // }
    //
    // for format in available {
    //     if format.format == vk::Format::B8G8R8A8_UNORM
    //         && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
    //     {
    //         return format.clone();
    //     }
    // }
    //
    // available_formats.first().unwrap().clone()
}

fn choose_mode(available: &[vk::PresentModeKHR]) -> vk::PresentModeKHR {
    let mut res = vk::PresentModeKHR::FIFO;

    for mode in available {
        if *mode == vk::PresentModeKHR::MAILBOX {
            return *mode;
        } else if *mode == vk::PresentModeKHR::IMMEDIATE {
            res = *mode;
        }
    }

    res
}

fn choose_extent(capabilities: &vk::SurfaceCapabilitiesKHR, size: (u32, u32)) -> vk::Extent2D {
    if capabilities.current_extent.width != u32::max_value() {
        capabilities.current_extent
    } else {
        fn clamp(num: u32, n: u32, x: u32) -> u32 {
            use std::cmp::{max, min};
            max(n, min(x, num))
        }

        vk::Extent2D {
            width: clamp(
                size.0,
                capabilities.min_image_extent.width,
                capabilities.max_image_extent.width,
            ),
            height: clamp(
                size.1,
                capabilities.min_image_extent.height,
                capabilities.max_image_extent.height,
            ),
        }
    }
}

pub struct SwapchainBase {
    pub loader: Swapchain,
    pub surface: SurfToken,
    pub chain: vk::SwapchainKHR,
    pub format: vk::Format,
    pub extent: vk::Extent2D,
    pub imgs: Vec<vk::Image>,
    pub depth_img: vk::Image,
    pub depth_img_fmt: vk::Format,
    pub depth_img_mem: vk::DeviceMemory,
    mem_prop: vk::PhysicalDeviceMemoryProperties,
    pdevice: vk::PhysicalDevice,
}

impl SwapchainBase {
    fn make_views(&self, dev: &Device) -> (Vec<vk::ImageView>, vk::ImageView) {
        (
            self.imgs
                .iter()
                .map(|&img| {
                    let create_view_info = vk::ImageViewCreateInfo::builder()
                        .view_type(vk::ImageViewType::TYPE_2D)
                        .format(self.format)
                        .components(vk::ComponentMapping {
                            r: vk::ComponentSwizzle::R,
                            g: vk::ComponentSwizzle::G,
                            b: vk::ComponentSwizzle::B,
                            a: vk::ComponentSwizzle::A,
                        })
                        .subresource_range(vk::ImageSubresourceRange {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            base_mip_level: 0,
                            level_count: 1,
                            base_array_layer: 0,
                            layer_count: 1,
                        })
                        .image(img);
                    unsafe { dev.create_image_view(&create_view_info, None).unwrap() }
                })
                .collect(),
            {
                let info = vk::ImageViewCreateInfo::builder()
                    .subresource_range(
                        vk::ImageSubresourceRange::builder()
                            .aspect_mask(vk::ImageAspectFlags::DEPTH)
                            .level_count(1)
                            .layer_count(1)
                            .build(),
                    )
                    .image(self.depth_img)
                    .format(self.depth_img_fmt)
                    .view_type(vk::ImageViewType::TYPE_2D);
                unsafe { dev.create_image_view(&info, None).unwrap() }
            },
        )
    }

    unsafe fn new(
        instance: &Instance,
        device: &Device,
        surface: SurfToken,
        pdevice: vk::PhysicalDevice,
        mem_prop: vk::PhysicalDeviceMemoryProperties,
        surf_size: (u32, u32),
    ) -> Self {
        let (swapchain, format, extent, imgs, loader, depth_img, depth_img_fmt, depth_img_mem) =
            Self::pregen(instance, device, &surface, pdevice, mem_prop, surf_size);
        Self {
            surface,
            chain: swapchain,
            format,
            extent,
            imgs,
            loader,
            depth_img,
            depth_img_fmt,
            depth_img_mem,
            mem_prop,
            pdevice,
        }
    }

    unsafe fn pregen(
        instance: &Instance,
        device: &Device,
        surface: &SurfToken,
        pdevice: vk::PhysicalDevice,
        mem_prop: vk::PhysicalDeviceMemoryProperties,
        surf_size: (u32, u32),
    ) -> (
        vk::SwapchainKHR,
        vk::Format,
        vk::Extent2D,
        Vec<vk::Image>,
        Swapchain,
        vk::Image,
        vk::Format,
        vk::DeviceMemory,
    ) {
        let (capabilities, formats, present_modes) = query_support(pdevice, &surface);
        let format = choose_format(&formats);
        let mode = choose_mode(&present_modes);
        let extent = choose_extent(&capabilities, surf_size);

        let mut desired_image_count = capabilities.min_image_count + 1;
        if capabilities.max_image_count > 0 && desired_image_count > capabilities.max_image_count {
            desired_image_count = capabilities.max_image_count;
        }
        let pre_transform = if capabilities
            .supported_transforms
            .contains(vk::SurfaceTransformFlagsKHR::IDENTITY)
        {
            vk::SurfaceTransformFlagsKHR::IDENTITY
        } else {
            capabilities.current_transform
        };

        let swapchain_loader = Swapchain::new(instance, device);
        let swapchain_create_info = vk::SwapchainCreateInfoKHR::builder()
            .surface(surface.surface)
            .min_image_count(desired_image_count)
            .image_color_space(format.color_space)
            .image_format(format.format)
            .image_extent(extent)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(mode)
            .clipped(true)
            .image_array_layers(1);
        let swapchain = swapchain_loader
            .create_swapchain(&swapchain_create_info, None)
            .unwrap();

        //println!("Making depth img");
        let (depth_img, depth_img_fmt, depth_img_mem) = {
            let fmt = vk::Format::D16_UNORM;
            let create_info = vk::ImageCreateInfo::builder()
                .image_type(vk::ImageType::TYPE_2D)
                .format(fmt)
                .extent(vk::Extent3D {
                    width: extent.width,
                    height: extent.height,
                    depth: 1,
                })
                .mip_levels(1)
                .array_layers(1)
                .samples(vk::SampleCountFlags::TYPE_1)
                .tiling(vk::ImageTiling::OPTIMAL)
                .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT)
                .sharing_mode(vk::SharingMode::EXCLUSIVE);
            let depth_img = device.create_image(&create_info, None).unwrap();

            let depth_image_memory_req = device.get_image_memory_requirements(depth_img);
            let depth_image_memory_index = find_memorytype_index(
                &depth_image_memory_req,
                &mem_prop,
                vk::MemoryPropertyFlags::DEVICE_LOCAL,
            )
            .expect("Unable to find suitable memory index for depth image.");

            let depth_image_allocate_info = vk::MemoryAllocateInfo::builder()
                .allocation_size(depth_image_memory_req.size)
                .memory_type_index(depth_image_memory_index);

            let depth_img_mem = device
                .allocate_memory(&depth_image_allocate_info, None)
                .unwrap();

            device
                .bind_image_memory(depth_img, depth_img_mem, 0)
                .unwrap();

            (depth_img, fmt, depth_img_mem)
        };
        //println!("Done making depth img");
        (
            swapchain,
            format.format,
            extent,
            swapchain_loader.get_swapchain_images(swapchain).unwrap(),
            swapchain_loader,
            depth_img,
            depth_img_fmt,
            depth_img_mem,
        )
    }

    unsafe fn recreate(&mut self, instance: &Instance, device: &Device, new_size: (u32, u32)) {
        let (swapchain, format, extent, imgs, loader, depth_img, depth_img_fmt, depth_img_mem) =
            Self::pregen(
                instance,
                device,
                &self.surface,
                self.pdevice,
                self.mem_prop,
                new_size,
            );
        self.chain = swapchain;
        self.format = format;
        self.extent = extent;
        self.imgs = imgs;
        self.loader = loader;
        self.depth_img = depth_img;
        self.depth_img_fmt = depth_img_fmt;
        self.depth_img_mem = depth_img_mem;
    }

    unsafe fn destroy(&mut self, dev: &Device) {
        //println!("Destroying SwapBase");
        self.loader.destroy_swapchain(self.chain, None);
        dev.free_memory(self.depth_img_mem, None);
        dev.destroy_image(self.depth_img, None);
    }
}

pub struct SwapToken {
    dev: Device,
    // pub surface_format: vk::SurfaceFormatKHR,
    // pub surface_resolution: vk::Extent2D,
    pub base: SwapchainBase,
    pub depth_view: vk::ImageView,
    pub img_views: Vec<vk::ImageView>,

    pub cmd_pool: CmdPool,

    pub renderpass: RenderPassToken,
    pub framebuffers: Vec<vk::Framebuffer>,

    pub viewport: vk::Viewport,
    pub scissors: vk::Rect2D,

    pub pipes: HashMap<String, PipeToken>,

    pub present_semaphore: vk::Semaphore,
    pub render_semaphore: vk::Semaphore,
    pub queue: vk::Queue,
    pub queue_fam: u32,
}

impl Drop for SwapToken {
    fn drop(&mut self) {
        unsafe {
            eprintln!("Dropping Swaptoken");
            //self.dev.device_wait_idle().unwrap();
            self.clean();
            self.dev.destroy_semaphore(self.present_semaphore, None);
            self.dev.destroy_semaphore(self.render_semaphore, None);
            //println!("Done Dropping Swaptoken")
        }
    }
}

impl SwapToken {
    unsafe fn clean(&mut self) {
        //println!("Cleaning Swaptoken");
        self.dev.device_wait_idle().unwrap();
        for buffer in self.framebuffers.drain(0..) {
            self.dev.destroy_framebuffer(buffer, None)
        }
        self.dev.destroy_image_view(self.depth_view, None);
        for img in self.img_views.drain(0..) {
            self.dev.destroy_image_view(img, None);
        }
        self.base.destroy(&self.dev);
    }

    pub fn recreate(
        &mut self,
        instance: &Instance,
        surf_size: (u32, u32),
        // r_data: &HashMap<String, PipeRecreationData>,
    ) {
        eprintln!("Recreating Swapchain");
        unsafe {
            self.dev.device_wait_idle().unwrap();
            self.clean();

            self.base.recreate(&instance, &self.dev, surf_size);
            let (depth_view, img_views, renderpass, framebuffers, viewport, scissors) =
                Self::create(&self.dev, &self.base, self.cmd_pool.buffers[0], self.queue);
            self.depth_view = depth_view;
            self.img_views = img_views;
            self.renderpass = renderpass;
            self.framebuffers = framebuffers;
            self.viewport = viewport;
            self.scissors = scissors;
            for (id, pipe) in self.pipes.iter_mut() {
                eprintln!("Recreating Pipeline: {}", id);
                pipe.recreate(self.renderpass.renderpass, self.viewport, self.scissors)
            }
        }
    }

    pub fn renderpass_begin_info(
        &self,
        present_index: u32,
        clear_values: &[vk::ClearValue],
    ) -> vk::RenderPassBeginInfo {
        vk::RenderPassBeginInfo::builder()
            .render_pass(self.renderpass.renderpass)
            .framebuffer(self.framebuffers[present_index as usize])
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: self.base.extent,
            })
            .clear_values(clear_values)
            .build()
    }

    pub fn new(
        instance: &Instance,
        dev: Device,
        surface: SurfToken,
        pdevice: vk::PhysicalDevice,
        queue_fam: u32,
        present_queue: vk::Queue,
        mem_prop: vk::PhysicalDeviceMemoryProperties,
        surf_size: (u32, u32),
    ) -> Self {
        let base =
            unsafe { SwapchainBase::new(instance, &dev, surface, pdevice, mem_prop, surf_size) };

        let cmd_pool = CmdPool::new(dev.clone(), queue_fam, 2);
        let (depth_view, img_views, renderpass, framebuffers, viewport, scissors) =
            Self::create(&dev, &base, cmd_pool.buffers[0], present_queue);

        let semaphore_create_info = vk::SemaphoreCreateInfo::default();

        Self {
            base,
            depth_view,
            img_views,
            cmd_pool,
            renderpass,
            framebuffers,
            viewport,
            scissors,
            pipes: HashMap::new(),
            present_semaphore: unsafe {
                dev.create_semaphore(&semaphore_create_info, None).unwrap()
            },
            render_semaphore: unsafe {
                dev.create_semaphore(&semaphore_create_info, None).unwrap()
            },
            queue: present_queue,
            queue_fam,
            dev,
        }
    }

    fn create(
        dev: &Device,
        base: &SwapchainBase,
        setup_command_buffer: vk::CommandBuffer,
        present_queue: vk::Queue,
    ) -> (
        vk::ImageView,
        Vec<vk::ImageView>,
        RenderPassToken,
        Vec<vk::Framebuffer>,
        vk::Viewport,
        vk::Rect2D,
    ) {
        let (img_views, depth_view) = base.make_views(&dev);
        let renderpass = RenderPassToken::new(dev.clone(), base.format);
        let framebuffers: Vec<vk::Framebuffer> = img_views
            .iter()
            .map(|present| {
                let attachments = [*present, depth_view];
                let frame_buffer_create_info = vk::FramebufferCreateInfo::builder()
                    .render_pass(renderpass.renderpass)
                    .attachments(&attachments)
                    .width(base.extent.width)
                    .height(base.extent.height)
                    .layers(1);

                unsafe {
                    dev.create_framebuffer(&frame_buffer_create_info, None)
                        .unwrap()
                }
            })
            .collect();
        let viewport = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: base.extent.width as _,
            height: base.extent.height as _,
            min_depth: 0.0,
            max_depth: 1.0,
        };
        let scissors = vk::Rect2D {
            extent: base.extent,
            ..Default::default()
        };

        unsafe {
            crate::command::record_submit_commandbuffer(
                dev,
                setup_command_buffer,
                present_queue,
                &[],
                &[],
                &[],
                |device, setup_command_buffer| {
                    let layout_transition_barriers = vk::ImageMemoryBarrier::builder()
                        .image(base.depth_img)
                        .dst_access_mask(
                            vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ
                                | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
                        )
                        .new_layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                        .old_layout(vk::ImageLayout::UNDEFINED)
                        .subresource_range(
                            vk::ImageSubresourceRange::builder()
                                .aspect_mask(vk::ImageAspectFlags::DEPTH)
                                .layer_count(1)
                                .level_count(1)
                                .build(),
                        );

                    device.cmd_pipeline_barrier(
                        setup_command_buffer,
                        vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                        vk::PipelineStageFlags::LATE_FRAGMENT_TESTS,
                        vk::DependencyFlags::empty(),
                        &[],
                        &[],
                        &[layout_transition_barriers.build()],
                    );
                },
            );
        }
        (
            depth_view,
            img_views,
            renderpass,
            framebuffers,
            viewport,
            scissors,
        )
    }

    pub fn make_pipeline<S: ToString>(
        &mut self,
        id: S,
        pool: DescPoolToken,
        shaders: HashMap<ShaderStage, ShaderArtifact>,
        push_consts: Vec<&PushConstant>,
        polygon_mode: vk::PolygonMode,
    ) {
        let pipe = PipeToken::build(
            self.dev.clone(),
            pool,
            push_consts,
            self.renderpass.renderpass,
            self.viewport,
            self.scissors,
            shaders,
            polygon_mode,
        );
        self.pipes.insert(id.to_string(), pipe);
    }

    pub unsafe fn prepare_render(
        &mut self,
        instance: &Instance,
        surf_size: (u32, u32),
    ) -> ash::prelude::VkResult<(u32, bool)> {
        let mut resized = false;
        loop {
            match self.base.loader.acquire_next_image(
                self.base.chain,
                std::u64::MAX,
                self.present_semaphore,
                vk::Fence::null(),
            ) {
                Ok(o) => break Ok((o.0, resized)),
                Err(r) if r == vk::Result::ERROR_OUT_OF_DATE_KHR => {
                    self.recreate(instance, surf_size);
                    resized = true;
                }
                Err(r) => break Err(r),
            }
        }
    }

    pub unsafe fn render<F>(
        &self,
        clear_color: [f32; 4],
        present_index: u32,
        render_fn: F,
    ) -> ash::prelude::VkResult<()>
    where
        F: FnOnce(vk::CommandBuffer),
    {
        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: clear_color,
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let render_pass_begin_info = self.renderpass_begin_info(present_index, &clear_values);

        self.cmd_pool.record(
            1,
            vk::CommandBufferUsageFlags::SIMULTANEOUS_USE,
            |dev, cmd_buffer| {
                dev.cmd_begin_render_pass(
                    cmd_buffer,
                    &render_pass_begin_info,
                    vk::SubpassContents::INLINE,
                );
                dev.cmd_set_viewport(cmd_buffer, 0, &[self.viewport]);
                dev.cmd_set_scissor(cmd_buffer, 0, &[self.scissors]);

                render_fn(cmd_buffer);

                dev.cmd_end_render_pass(cmd_buffer);
            },
        );

        self.cmd_pool.submit(
            self.queue,
            &[vk::PipelineStageFlags::BOTTOM_OF_PIPE],
            &[self.present_semaphore],
            &[self.render_semaphore],
            &[1],
        );

        //eprintln!("Render Time: {:?}", render_time.elapsed());

        let present_info = vk::PresentInfoKHR {
            wait_semaphore_count: 1,
            p_wait_semaphores: &self.render_semaphore,
            swapchain_count: 1,
            p_swapchains: &self.base.chain,
            p_image_indices: &present_index,
            ..Default::default()
        };
        self.base.loader.queue_present(self.queue, &present_info)?;
        Ok(())
    }
}
