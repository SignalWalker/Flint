use crate::buffer::BufToken;
use crate::vertex::polyhedron::*;
use crate::vertex::Vertex;
use ash::version::DeviceV1_0;
use ash::vk;
use ash::Device;
use std::fmt::Debug;

pub struct Model {
    pub vert_buf: BufToken,
    pub ind_buf: BufToken,
    pub face_count: u32,
}

impl Debug for Model {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("Model")
            .field("face_count", &self.face_count)
            .finish()
    }
}

impl From<collada::Vertex> for Vertex {
    fn from(c: collada::Vertex) -> Self {
        Self::from([c.x as _, c.y as _, c.z as _, 0.0, 0.0])
    }
}

impl Model {
    pub fn new(
        dev: Device,
        mem_prop: &vk::PhysicalDeviceMemoryProperties,
        shape: Polyhedron<Vertex>,
    ) -> Self {
        let (vert_buf, ind_buf) = shape.make_buffers(dev, mem_prop);
        Self {
            vert_buf,
            ind_buf,
            face_count: shape.faces.len() as u32,
        }
    }

    pub fn bind(&self, cmd_buffer: vk::CommandBuffer) {
        unsafe {
            let dev = &self.vert_buf.dev;
            dev.cmd_bind_vertex_buffers(cmd_buffer, 0, &[self.vert_buf.buf], &[0]);
            dev.cmd_bind_index_buffer(cmd_buffer, self.ind_buf.buf, 0, vk::IndexType::UINT16);
        }
    }

    pub fn draw(&self, cmd_buffer: vk::CommandBuffer) {
        unsafe {
            self.vert_buf
                .dev
                .cmd_draw_indexed(cmd_buffer, self.face_count * 3, 1, 0, 0, 1);
        }
    }
}
