use crate::buffer::BufToken;
use crate::vertex::Vertex;
use ash::vk;
use ash::Device;
use nalgebra::{Matrix3, Matrix4, Vector3, Vector4};
use std::path::Path;

use rayon::prelude::*;
use std::iter::FromIterator;
use std::ops::{Index, IndexMut};

#[derive(Clone, Debug)]
pub struct Polyhedron<P> {
    pub points: Vec<P>,
    pub faces: Vec<[u16; 3]>,
}

impl<P> Polyhedron<P> {
    pub fn map<O, F>(self, f: F) -> Polyhedron<O>
    where
        F: Fn(P) -> O,
    {
        Polyhedron {
            points: self.points.into_iter().map(f).collect::<Vec<_>>(),
            faces: self.faces,
        }
    }

    pub fn par_map<O: Send, F>(self, f: F) -> Polyhedron<O>
    where
        P: Send,
        F: Fn(P) -> O + Sync + Send,
    {
        Polyhedron {
            points: self.points.into_par_iter().map(f).collect::<Vec<_>>(),
            faces: self.faces,
        }
    }

    pub fn par_for_each<F>(&mut self, f: F)
    where
        P: Send,
        F: Fn(&mut P) + Sync + Send,
    {
        self.points.par_iter_mut().for_each(f)
    }

    pub fn from<O: Into<P>>(o: Polyhedron<O>) -> Self {
        o.map(Into::into)
    }

    pub fn into<O>(self) -> Polyhedron<O>
    where
        P: Into<O>,
    {
        self.map(Into::into)
    }

    pub fn points(&self) -> std::slice::Iter<'_, P> {
        self.points.iter()
    }

    pub fn points_mut(&mut self) -> std::slice::IterMut<'_, P> {
        self.points.iter_mut()
    }

    pub fn faces(&self) -> std::slice::Iter<'_, [u16; 3]> {
        self.faces.iter()
    }

    pub fn faces_mut(&mut self) -> std::slice::IterMut<'_, [u16; 3]> {
        self.faces.iter_mut()
    }
}

impl<P> Index<usize> for Polyhedron<P> {
    type Output = P;
    fn index(&self, i: usize) -> &P {
        &self.points[i]
    }
}

impl<P> IndexMut<usize> for Polyhedron<P> {
    fn index_mut(&mut self, i: usize) -> &mut P {
        &mut self.points[i]
    }
}

impl<P: PartialEq + Clone> FromIterator<[P; 3]> for Polyhedron<P> {
    fn from_iter<I: IntoIterator<Item = [P; 3]>>(iter: I) -> Self {
        let mut points = Vec::new();
        let faces =
            iter.into_iter()
                .map(|tri| {
                    let mut map = tri.iter().map(|p| {
                        match points.iter().enumerate().find(|(_, o)| *p == **o) {
                            Some((i, _)) => i,
                            None => {
                                points.push(p.clone());
                                points.len() - 1
                            }
                        }
                    });
                    [
                        map.next().unwrap() as u16,
                        map.next().unwrap() as u16,
                        map.next().unwrap() as u16,
                    ]
                })
                .collect::<Vec<_>>();
        Self { points, faces }
    }
}

impl Polyhedron<Vertex> {
    pub fn transform(&mut self, mat: &Matrix4<f32>) {
        self.par_for_each(|v| {
            let p = *mat * Vector4::new(v[0], v[1], v[2], 1.0);
            v[0] = p.x;
            v[1] = p.y;
            v[2] = p.z;
        });
    }

    pub fn calc_normals(&mut self) {
        for point in self.points.iter_mut() {
            point[5] = 0.0;
            point[6] = 0.0;
            point[7] = 0.0;
        }
        for (a, b, c) in self
            .faces
            .iter()
            .map(|tri| (tri[0] as usize, tri[1] as usize, tri[2] as usize))
        {
            let (a_v, b_v, c_v) = {
                let a = &self.points[a];
                let b = &self.points[b];
                let c = &self.points[c];
                (
                    Vector3::new(a[0], a[1], a[2]),
                    Vector3::new(b[0], b[1], b[2]),
                    Vector3::new(c[0], c[1], c[2]),
                )
            };
            let norm = (b_v - a_v).cross(&(c_v - a_v));
            self.points[a].add_norm(norm);
            self.points[b].add_norm(norm);
            self.points[c].add_norm(norm);
        }
        for point in self.points.iter_mut() {
            let norm = Vector3::new(point[5], point[6], point[7]).normalize();
            point[5] = norm.x;
            point[6] = norm.y;
            point[7] = norm.z;
            // eprintln!("[{}, {}, {}]", norm.x, norm.y, norm.z);
        }
    }

    pub fn transform_uv(&mut self, mat: &Matrix3<f32>) {
        self.par_for_each(|v| {
            let p = *mat * Vector3::new(v[3], v[4], 1.0);
            v[3] = p.x;
            v[4] = p.y;
        });
    }

    pub fn make_buffers(
        &self,
        dev: Device,
        mem_prop: &vk::PhysicalDeviceMemoryProperties,
    ) -> (BufToken, BufToken) {
        (
            // Vertex Buffer
            BufToken::with_data(
                dev.clone(),
                vk::BufferUsageFlags::VERTEX_BUFFER,
                vk::SharingMode::EXCLUSIVE,
                mem_prop,
                &self.points,
            ),
            // Index
            BufToken::with_data(
                dev,
                vk::BufferUsageFlags::INDEX_BUFFER,
                vk::SharingMode::EXCLUSIVE,
                mem_prop,
                &self.faces,
            ),
        )
    }

    pub fn merge(&mut self, mut other: Self) {
        let offset = self.points.len() as u16;
        self.points.append(&mut other.points);
        for (a, b, c) in other.faces.iter().map(|tri| (tri[0], tri[1], tri[2])) {
            self.faces.push([a + offset, b + offset, c + offset]);
        }
    }

    pub fn point() -> Self {
        Polyhedron {
            points: vec![[0.0, 0.0, 0.0].into()],
            faces: vec![[0, 0, 0]],
        }
    }

    pub fn cube() -> Self {
        Polyhedron {
            points: vec![
                // back
                [0.5, -0.5, -0.5, 1.0, 0.0].into(),
                [-0.5, -0.5, -0.5, 0.0, 0.0].into(),
                [-0.5, 0.5, -0.5, 0.0, 1.0].into(),
                [0.5, 0.5, -0.5, 1.0, 1.0].into(),
                // front
                [-0.5, -0.5, 0.5, 0.0, 0.0].into(),
                [0.5, -0.5, 0.5, 1.0, 0.0].into(),
                [0.5, 0.5, 0.5, 1.0, 1.0].into(),
                [-0.5, 0.5, 0.5, 0.0, 1.0].into(),
            ],
            faces: vec![
                // back
                [0, 1, 2],
                [2, 3, 0],
                // front
                [4, 5, 6],
                [6, 7, 4],
                // bottom
                [5, 4, 1],
                [1, 0, 5],
                // top
                [3, 2, 7],
                [7, 6, 3],
                // left
                [1, 4, 7],
                [7, 2, 1],
                // right
                [5, 0, 3],
                [3, 6, 5],
            ],
        }
    }

    pub fn tri() -> Self {
        Polyhedron {
            points: vec![
                [0.5, 0.0, -0.5, 1.0, 0.0].into(),
                [-0.5, 0.0, -0.5, 0.0, 0.0].into(),
                [-0.5, 0.0, 0.5, 0.0, 1.0].into(),
            ],
            faces: vec![[0, 1, 2]],
        }
    }

    pub fn quad() -> Self {
        Polyhedron {
            points: vec![
                [0.5, 0.0, -0.5, 1.0, 0.0].into(),
                [-0.5, 0.0, -0.5, 0.0, 0.0].into(),
                [-0.5, 0.0, 0.5, 0.0, 1.0].into(),
                [0.5, 0.0, 0.5, 1.0, 1.0].into(),
            ],
            faces: vec![[0, 1, 2], [2, 3, 0]],
        }
    }

    pub fn ramp_tri() -> Self {
        Polyhedron {
            points: vec![
                [0.5, -0.25, -0.5, 1.0, 0.0].into(),
                [-0.5, -0.25, -0.5, 0.0, 0.0].into(),
                [-0.5, 0.25, 0.5, 0.0, 1.0].into(),
            ],
            faces: vec![[0, 1, 2]],
        }
    }

    pub fn ramp_quad() -> Self {
        Polyhedron {
            points: vec![
                [0.5, -0.25, -0.5, 1.0, 0.0].into(),
                [-0.5, -0.25, -0.5, 0.0, 0.0].into(),
                [-0.5, 0.25, 0.5, 0.0, 1.0].into(),
                [0.5, 0.25, 0.5, 1.0, 1.0].into(),
            ],
            faces: vec![[0, 1, 2], [2, 3, 0]],
        }
    }

    pub fn load<P: AsRef<Path>>(path: P) -> Self {
        use collada::{document, PrimitiveElement};
        let doc = document::ColladaDocument::from_path(path.as_ref()).unwrap();
        // dbg!(doc.get_obj_set());
        let mut obj = doc.get_obj_set().unwrap().objects.remove(0);
        let mut points: Vec<Vertex> = obj
            .vertices
            .drain(0..)
            .map(std::convert::Into::into)
            .collect();
        let uv: Vec<[f32; 2]> = obj
            .tex_vertices
            .drain(0..)
            .map(|tex| [tex.x as _, tex.y as _])
            .collect();
        let norms: Vec<[f32; 3]> = obj
            .normals
            .drain(0..)
            .map(|norm| [norm.x as _, norm.y as _, norm.z as _])
            .collect();
        let faces: Vec<[u16; 3]> = match obj.geometry.remove(0).mesh.remove(0) {
            PrimitiveElement::Polylist(_) => panic!("Not Implemented"),
            PrimitiveElement::Triangles(t) => {
                let mut res = Vec::new();
                for (vert, tex, norm) in t.vertices.iter().enumerate().map(|(i, v)| {
                    (
                        [v.0 as u16, v.1 as u16, v.2 as u16],
                        t.tex_vertices.as_ref().map(|t| t[i]),
                        t.normals.as_ref().map(|n| n[i]),
                    )
                }) {
                    let tex = tex.map(|t| [t.0, t.1, t.2]);
                    if let Some(t) = tex {
                        for (v, t) in vert.iter().zip(t.iter()) {
                            points[*v as usize].uv_mut().copy_from_slice(&uv[*t]);
                        }
                    }
                    let norm = norm.map(|n| [n.0, n.1, n.2]);
                    if let Some(n) = norm {
                        for (v, n) in vert.iter().zip(n.iter()) {
                            points[*v as usize].norm_mut().copy_from_slice(&norms[*n]);
                        }
                    }
                    res.push(vert);
                }
                res
            }
        };
        Self { points, faces }
    }

    pub fn inertia_moment(&self) -> Matrix3<f32> {
        unimplemented!()
    }
}
