use crate::*;
use ash::extensions::khr::Swapchain;
use ash::version::{DeviceV1_0, EntryV1_0, InstanceV1_0};
use ash::{vk, Device, Entry, Instance};

use std::default::Default;
use std::ffi::CString;
use std::ops::Drop;

pub mod surface;
pub mod swapchain;
use ash::prelude::*;
pub use surface::*;
pub use swapchain::*;

pub struct VkContext {
    pub entry: Entry,
    pub instance: Instance,

    pub debug_report_loader: DebugReport,
    pub debug_call_back: vk::DebugReportCallbackEXT,

    pub device: Device,
    pub pdevice: vk::PhysicalDevice,
    pub device_memory_properties: vk::PhysicalDeviceMemoryProperties,
}

impl VkContext {
    pub fn builder() -> InstanceBuilder {
        InstanceBuilder::default()
    }
}

impl Drop for VkContext {
    fn drop(&mut self) {
        println!("Dropping VkData");
        unsafe {
            self.device.device_wait_idle().unwrap();
            self.device.destroy_device(None);
            self.debug_report_loader
                .destroy_debug_report_callback(self.debug_call_back, None);
            self.instance.destroy_instance(None);
            //println!("Dropped VkData");
        }
    }
}

pub struct InstanceBuilder {
    pub app_name: Option<String>,
    pub app_version: u32,
    pub layers: Vec<CString>,
    pub api_version: (u32, u32, u32),
}

impl Default for InstanceBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl InstanceBuilder {
    fn new() -> Self {
        InstanceBuilder {
            app_name: None,
            app_version: 0,
            layers: Vec::new(),
            api_version: (1, 0, 0),
        }
    }

    pub fn with_layer<S: AsRef<str>>(mut self, layer: S) -> Self {
        self.layers.push(CString::new(layer.as_ref()).unwrap());
        self
    }

    pub fn with_validation_layer(self) -> Self {
        self.with_layer("VK_LAYER_LUNARG_standard_validation")
    }

    pub fn with_api_version(mut self, major: u32, minor: u32, patch: u32) -> Self {
        self.api_version = (major, minor, patch);
        self
    }

    pub fn with_app_version(mut self, version: u32) -> Self {
        self.app_version = version;
        self
    }

    pub fn with_name<S: ToString>(mut self, name: S) -> Self {
        self.app_name = Some(name.to_string());
        self
    }

    pub unsafe fn prepare_context(mut self) -> VkResult<ContextBuilder> {
        let entry = Entry::new().unwrap();
        let app_name = CString::new(match self.app_name.take() {
            Some(s) => s,
            None => "Flint App".to_string(),
        })
        .unwrap();

        let layers_names_raw: Vec<*const i8> = self
            .layers
            .iter()
            .map(|raw_name| raw_name.as_ptr())
            .collect();

        // TODO :: with_extension()
        let extension_names_raw = extension_names();

        let appinfo = vk::ApplicationInfo::builder()
            .application_name(&app_name)
            .application_version(self.app_version)
            .engine_name(&app_name)
            .engine_version(0) // TODO :: Engine Version
            .api_version(ash::vk::make_version(
                self.api_version.0,
                self.api_version.1,
                self.api_version.2,
            ));

        let create_info = vk::InstanceCreateInfo::builder()
            .application_info(&appinfo)
            .enabled_layer_names(&layers_names_raw)
            .enabled_extension_names(&extension_names_raw);

        let instance: Instance = entry
            .create_instance(&create_info, None)
            .expect("Instance creation error");

        let debug_info = vk::DebugReportCallbackCreateInfoEXT::builder()
            .flags(
                vk::DebugReportFlagsEXT::ERROR
                    | vk::DebugReportFlagsEXT::WARNING
                    | vk::DebugReportFlagsEXT::PERFORMANCE_WARNING, //| vk::DebugReportFlagsEXT::INFORMATION,
            )
            .pfn_callback(Some(vulkan_debug_callback));

        let debug_report_loader = DebugReport::new(&entry, &instance);
        let debug_call_back = debug_report_loader
            .create_debug_report_callback(&debug_info, None)
            .unwrap();

        Ok(ContextBuilder {
            entry: Some(entry),
            instance: Some(instance),
            debug_call_back: Some(debug_call_back),
            debug_report_loader: Some(debug_report_loader),
            phys_dev: None,
        })
    }
}

pub struct ContextBuilder {
    entry: Option<Entry>,
    instance: Option<Instance>,
    debug_report_loader: Option<DebugReport>,
    debug_call_back: Option<vk::DebugReportCallbackEXT>,
    pub phys_dev: Option<vk::PhysicalDevice>,
}

impl Drop for ContextBuilder {
    fn drop(&mut self) {
        println!("Dropping ContextBuilder");
        unsafe {
            if let Some(d) = self.debug_report_loader.take() {
                d.destroy_debug_report_callback(self.debug_call_back.take().unwrap(), None)
            }
            if let Some(i) = self.instance.take() {
                i.destroy_instance(None);
            }
        }
    }
}

impl ContextBuilder {
    pub unsafe fn get_devices(&self) -> Vec<vk::PhysicalDevice> {
        self.instance
            .as_ref()
            .unwrap()
            .enumerate_physical_devices()
            .expect("Physical device error")
    }

    pub unsafe fn get_dev_ext_props(
        &self,
        pdevice: vk::PhysicalDevice,
    ) -> VkResult<Vec<vk::ExtensionProperties>> {
        self.instance
            .as_ref()
            .unwrap()
            .enumerate_device_extension_properties(pdevice)
    }

    pub unsafe fn get_dev_props(
        &self,
        pdevice: vk::PhysicalDevice,
    ) -> vk::PhysicalDeviceProperties {
        self.instance
            .as_ref()
            .unwrap()
            .get_physical_device_properties(pdevice)
    }

    pub unsafe fn get_all_dev_props(
        &self,
    ) -> Vec<(vk::PhysicalDevice, vk::PhysicalDeviceProperties)> {
        self.get_devices()
            .iter()
            .map(|dev| (*dev, self.get_dev_props(*dev)))
            .collect::<Vec<_>>()
    }

    pub fn with_device(mut self, dev: vk::PhysicalDevice) -> Self {
        self.phys_dev = Some(dev);
        self
    }

    pub fn with_device_by_type(mut self, ty: vk::PhysicalDeviceType) -> (Self, bool) {
        for (dev, props) in unsafe { self.get_all_dev_props() } {
            if props.device_type == ty {
                self.phys_dev = Some(dev);
                return (self, true)
            }
        }
        (self, false)
    }

    pub fn with_device_by_name<S: AsRef<str>>(mut self, name: S) -> (Self, bool) {
        let name = name.as_ref();
        for (dev, props) in unsafe { self.get_all_dev_props() } {
            if &props.name()[..] == name {
                self.phys_dev = Some(dev);
                return (self, true);
            }
        }
        (self, false)
    }

    pub fn with_device_by_index(mut self, index: usize) -> (Self, bool) {
        let dev = match unsafe { self.get_devices() }.get(index) {
            Some(d) => *d,
            None => return (self, false),
        };
        self.phys_dev = Some(dev);
        (self, true)
    }

    pub fn build_with_swapchain(
        mut self,
        window: &winit::window::Window,
    ) -> VkResult<(VkContext, SwapToken)> {
        unsafe {
            let pdevices = self.get_devices();
            let surface = SurfToken::new(
                self.entry.as_ref().unwrap(),
                self.instance.as_ref().unwrap(),
                &window,
            );
            let (pdevice, queue_family_index) = pdevices
                .iter()
                .map(|pdevice| {
                    self.instance
                        .as_ref()
                        .unwrap()
                        .get_physical_device_queue_family_properties(*pdevice)
                        .iter()
                        .enumerate()
                        .filter_map(|(index, ref info)| {
                            let supports_graphic_and_surface =
                                info.queue_flags.contains(vk::QueueFlags::GRAPHICS)
                                    && surface
                                        .loader
                                        .get_physical_device_surface_support(
                                            *pdevice,
                                            index as u32,
                                            surface.surface,
                                        )
                                        .unwrap(); // TODO :: Handle this result
                            if supports_graphic_and_surface {
                                Some((*pdevice, index))
                            } else {
                                None
                            }
                        })
                        .nth(0)
                })
                .filter_map(|v| v)
                .nth(0)
                .expect("Couldn't find suitable device.");

            let queue_family_index = queue_family_index as u32;
            let device_extension_names_raw = [Swapchain::name().as_ptr()];
            let features = vk::PhysicalDeviceFeatures {
                shader_clip_distance: 1,
                fill_mode_non_solid: 1,
                ..Default::default()
            };
            let priorities = [1.0];

            let device: Device = {
                let queue_info = [vk::DeviceQueueCreateInfo::builder()
                    .queue_family_index(queue_family_index)
                    .queue_priorities(&priorities)
                    .build()];

                let device_create_info = vk::DeviceCreateInfo::builder()
                    .queue_create_infos(&queue_info)
                    .enabled_extension_names(&device_extension_names_raw)
                    .enabled_features(&features);
                self.instance
                    .as_ref()
                    .unwrap()
                    .create_device(pdevice, &device_create_info, None)?
            };

            let present_queue = device.get_device_queue(queue_family_index as u32, 0);

            let device_memory_properties = self
                .instance
                .as_ref()
                .unwrap()
                .get_physical_device_memory_properties(pdevice);
            let swapchain = SwapToken::new(
                self.instance.as_ref().unwrap(),
                device.clone(),
                surface,
                pdevice,
                queue_family_index,
                present_queue,
                device_memory_properties,
                window.inner_size().into(),
            );
            Ok((
                VkContext {
                    entry: self.entry.take().unwrap(),
                    instance: self.instance.take().unwrap(),
                    debug_call_back: self.debug_call_back.take().unwrap(),
                    debug_report_loader: self.debug_report_loader.take().unwrap(),
                    device,
                    pdevice,
                    device_memory_properties,
                },
                swapchain,
            ))
        }
    }
}

pub fn chars_to_string(name: &[c_char]) -> String {
    let name = unsafe { std::mem::transmute::<&[c_char], &[u8]>(name) };
    let name = &name[0..name.iter().position(|c| *c == 0).unwrap()];
    std::str::from_utf8(name).unwrap().to_string()
}

pub trait DeviceProperties {
    fn name(&self) -> String;
    fn supported_version(&self) -> (u32, u32, u32);
}

impl DeviceProperties for vk::PhysicalDeviceProperties {
    fn name(&self) -> String {
        chars_to_string(&self.device_name)
    }
    fn supported_version(&self) -> (u32, u32, u32) {
        (
            vk::version_major(self.api_version),
            vk::version_minor(self.api_version),
            vk::version_patch(self.api_version),
        )
    }
}

pub trait DeviceExtension {
    fn name(&self) -> String;
}

impl DeviceExtension for vk::ExtensionProperties {
    fn name(&self) -> String {
        chars_to_string(&self.extension_name)
    }
}

// pub fn device_supported_version(encoded: u32)

#[cfg(test)]
mod tests {

    use crate::base::*;

    #[test]
    fn get_device_names() {
        let devices = unsafe {
            let builder = InstanceBuilder::new()
                .with_api_version(1, 2, 141)
                .prepare_context()
                .unwrap();
            builder
                .get_devices()
                .iter()
                .map(|dev| builder.get_dev_props(*dev).name())
                .collect::<Vec<_>>()
        };
        dbg!(devices);
    }

    #[test]
    fn get_device_extensions() {
        let devices = unsafe {
            let builder = InstanceBuilder::new()
                .with_api_version(1, 2, 141)
                .prepare_context()
                .unwrap();
            builder
                .get_devices()
                .iter()
                .map(|dev| {
                    (
                        builder.get_dev_props(*dev).name(),
                        builder
                            .get_dev_ext_props(*dev)
                            .unwrap()
                            .iter()
                            .map(|ext| ext.name())
                            .collect::<Vec<_>>(),
                    )
                })
                .collect::<Vec<_>>()
        };
        dbg!(devices);
    }
}
