use ash::vk;
use nalgebra::Vector3;
use std::ops::{Index, IndexMut};

pub mod model;
mod polyhedron;
pub use polyhedron::*;

pub trait VertInfo {
    fn bind_descs() -> Vec<vk::VertexInputBindingDescription>;
    fn attr_descs() -> Vec<vk::VertexInputAttributeDescription>;
    fn input_state_info(
        attr: &[vk::VertexInputAttributeDescription],
        bind: &[vk::VertexInputBindingDescription],
    ) -> vk::PipelineVertexInputStateCreateInfo;
    fn asm_state_info() -> vk::PipelineInputAssemblyStateCreateInfo;
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Vertex([f32; 11]);

impl Default for Vertex {
    fn default() -> Self {
        Self([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0])
    }
}

impl Vertex {
    pub fn pos(&self) -> &[f32] {
        &self.0[..3]
    }

    pub fn pos_mut(&mut self) -> &mut [f32] {
        &mut self.0[..3]
    }

    pub fn uv(&self) -> &[f32] {
        &self.0[3..5]
    }

    pub fn uv_mut(&mut self) -> &mut [f32] {
        &mut self.0[3..5]
    }

    pub fn norm(&self) -> &[f32] {
        &self.0[5..8]
    }

    pub fn norm_mut(&mut self) -> &mut [f32] {
        &mut self.0[5..8]
    }

    pub fn color(&self) -> &[f32] {
        &self.0[8..11]
    }

    pub fn color_mut(&mut self) -> &mut [f32] {
        &mut self.0[8..11]
    }

    pub fn add_norm(&mut self, norm: Vector3<f32>) {
        self[5] += norm.x;
        self[6] += norm.y;
        self[7] += norm.z;
    }
}

impl Index<usize> for Vertex {
    type Output = f32;
    fn index(&self, i: usize) -> &Self::Output {
        &self.0[i]
    }
}

impl IndexMut<usize> for Vertex {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        &mut self.0[i]
    }
}

impl From<[f32; 3]> for Vertex {
    fn from(arr: [f32; 3]) -> Self {
        Self([
            arr[0], arr[1], arr[2], 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,
        ])
    }
}

impl From<[f32; 5]> for Vertex {
    fn from(arr: [f32; 5]) -> Self {
        Self([
            arr[0], arr[1], arr[2], arr[3], arr[4], 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,
        ])
    }
}

impl From<[f32; 8]> for Vertex {
    fn from(arr: [f32; 8]) -> Self {
        Self([
            arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], 1.0, 1.0, 1.0,
        ])
    }
}

impl From<[f32; 11]> for Vertex {
    fn from(arr: [f32; 11]) -> Self {
        Self(arr)
    }
}

impl VertInfo for Vertex {
    fn bind_descs() -> Vec<vk::VertexInputBindingDescription> {
        vec![vk::VertexInputBindingDescription {
            binding: 0,
            stride: std::mem::size_of::<Self>() as u32,
            input_rate: vk::VertexInputRate::VERTEX,
        }]
    }

    fn attr_descs() -> Vec<vk::VertexInputAttributeDescription> {
        vec![
            vk::VertexInputAttributeDescription {
                location: 0,
                binding: 0,
                // Format looks like a color, but it's actually a
                // description of how the attribute is formatted.
                // R32G32B32_SFLOAT because it's a Vec3 of f32
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 0, //offset_of!(Self, pos) as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 1,
                binding: 0,
                format: vk::Format::R32G32_SFLOAT,
                offset: 3 * std::mem::size_of::<f32>() as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 2,
                binding: 0,
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 5 * std::mem::size_of::<f32>() as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 3,
                binding: 0,
                format: vk::Format::R32G32B32_SFLOAT,
                offset: 8 * std::mem::size_of::<f32>() as u32,
            },
        ]
    }

    fn input_state_info(
        attr: &[vk::VertexInputAttributeDescription],
        bind: &[vk::VertexInputBindingDescription],
    ) -> vk::PipelineVertexInputStateCreateInfo {
        vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_attribute_descriptions(attr)
            .vertex_binding_descriptions(bind)
            .build()
    }

    fn asm_state_info() -> vk::PipelineInputAssemblyStateCreateInfo {
        vk::PipelineInputAssemblyStateCreateInfo {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            ..Default::default()
        }
    }
}
