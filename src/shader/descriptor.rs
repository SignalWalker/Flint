use crate::buffer::*;
use crate::shader::*;
use ash::{
    vk,
    vk::{DescriptorSet, DescriptorSetLayoutBinding, DescriptorType, ShaderStageFlags},
    Device,
};
use spirv_cross::*;
use std::fmt::Debug;

mod pool;
mod push_constant;
mod set;
pub use pool::*;
pub use push_constant::*;
pub use set::*;

#[derive(Debug, Clone)]
pub struct DescField {
    pub index: usize,
    pub offset: vk::DeviceSize,
    pub size: vk::DeviceSize,
    pub ty: DescriptorType,
    pub count: usize,
}

impl DescField {
    fn from_desc_res<T>(
        ast: &spirv::Ast<T>,
        block_type: &spirv::Type,
        res: &spirv::Resource,
        min_offset: u64,
    ) -> HashMap<String, Self>
    where
        T: spirv::Target,
        spirv::Ast<T>: spirv::Compile<T> + spirv::Parse<T>,
    {
        let mut fields = HashMap::new();
        if let spirv::Type::Struct { member_types, .. } = block_type {
            for (i, type_id) in member_types.iter().enumerate() {
                fields.insert(
                    ast.get_member_name(res.base_type_id, i as _).unwrap(),
                    Self::new(ast, res, *type_id, i, min_offset),
                );
            }
        }
        fields
    }

    fn new<T>(
        ast: &spirv::Ast<T>,
        res: &spirv::Resource,
        type_id: u32,
        index: usize,
        min_offset: u64,
    ) -> Self
    where
        T: spirv::Target,
        spirv::Ast<T>: spirv::Compile<T> + spirv::Parse<T>,
    {
        let (ty, count) = cross_to_ash(&ast.get_type(type_id).unwrap());
        let mut offset = u64::from(
            ast.get_member_decoration(res.base_type_id, index as _, spirv::Decoration::Offset)
                .unwrap(),
        );
        let size = ast
            .get_declared_struct_member_size(res.type_id, index as _)
            .unwrap();
        // Offset must be multiple of min_offset
        let modulo = offset % min_offset;
        if modulo != 0 {
            offset += min_offset - modulo;
        }
        DescField {
            index,
            offset,
            size: size.into(),
            ty,
            count,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Descriptor {
    //pub size: vk::DeviceSize,
    pub set: u32,
    pub binding: u32,
    pub name: String,
    pub ty: DescriptorType,
    pub count: usize,
    pub stage: ShaderStageFlags,
    pub fields: HashMap<String, DescField>,
}

impl Into<DescriptorSetLayoutBinding> for Descriptor {
    fn into(self) -> DescriptorSetLayoutBinding {
        DescriptorSetLayoutBinding {
            binding: self.binding,
            descriptor_type: self.ty,
            descriptor_count: self.count as _,
            stage_flags: self.stage,
            p_immutable_samplers: std::ptr::null(), // TODO - Whatever this is
        }
    }
}

impl Descriptor {
    pub fn make_buffer(
        &self,
        dev: Device,
        mem_prop: &vk::PhysicalDeviceMemoryProperties,
    ) -> Buffer {
        use Buffer::*;
        if self.fields.is_empty() {
            panic!("Not Implemented: Buffer from non-struct descriptor");
        // Raw(BufToken::with_size(
        //     dev,
        //     vk::BufferUsageFlags::UNIFORM_BUFFER,
        //     vk::SharingMode::EXCLUSIVE,
        //     mem_prop,
        //     self.size,
        // ))
        } else {
            Struct(BufStruct::from_fields(
                dev,
                vk::BufferUsageFlags::UNIFORM_BUFFER,
                vk::SharingMode::EXCLUSIVE,
                mem_prop,
                self.fields
                    .iter()
                    .map(|(id, field)| (id.clone(), field.offset, field.size)),
            ))
        }
    }

    pub fn new<T>(
        ast: &spirv::Ast<T>,
        stage: ShaderStageFlags,
        res: &spirv::Resource,
        min_offset: vk::DeviceSize,
    ) -> Self
    where
        T: spirv::Target,
        spirv::Ast<T>: spirv::Compile<T> + spirv::Parse<T>,
    {
        // TODO :: Repack UBOs to optimize memory (Because min_offset might not be a factor of the sum of sizes of UBO fields)
        let desc_type = ast.get_type(res.type_id).unwrap();
        let (ty, count) = cross_to_ash(&desc_type);
        Descriptor {
            set: ast
                .get_decoration(res.id, spirv::Decoration::DescriptorSet)
                .unwrap(),
            name: res.name.clone(),
            binding: ast
                .get_decoration(res.id, spirv::Decoration::Binding)
                .unwrap(),
            ty,
            count,
            stage,
            fields: DescField::from_desc_res(&ast, &desc_type, &res, min_offset),
        }
    }

    /// Generates `vk::DescriptorPoolSize`, so descriptor pools can automatically size themselves.
    pub fn size(&self) -> vk::DescriptorPoolSize {
        vk::DescriptorPoolSize {
            ty: self.ty,
            descriptor_count: self.count as _,
        }
    }

    /// Generates a `vk::WriteDescriptorSet` for `vk::Device::update_descriptor_sets`.
    /// Used by `DescPoolToken::update_desc_sets`.
    pub fn make_write(&self, set: DescriptorSet, info: &DescWriteInfo) -> vk::WriteDescriptorSet {
        use DescWriteInfo::*;
        let mut res = vk::WriteDescriptorSet {
            dst_set: set,
            dst_binding: self.binding,
            descriptor_count: self.count as _,
            descriptor_type: self.ty,
            ..Default::default()
        };
        match info {
            Buf(buf) => res.p_buffer_info = buf,
            Img(img) => res.p_image_info = img,
            Tex(tex) => res.p_texel_buffer_view = tex,
        }
        res
    }
}

/// Used in `Descriptor::make_write` to determine what kind of
/// `vk::WriteDescriptorSet` to make.
#[derive(Copy, Clone, Debug)]
pub enum DescWriteInfo {
    Buf(vk::DescriptorBufferInfo),
    Img(vk::DescriptorImageInfo),
    Tex(vk::BufferView),
}

pub fn cross_to_ash(c: &spirv::Type) -> (DescriptorType, usize) {
    use spirv::Type::*;
    //dbg!(c);
    let mut res = match c {
        Image { array } => (DescriptorType::SAMPLED_IMAGE, array.len()),
        SampledImage { array } => (DescriptorType::COMBINED_IMAGE_SAMPLER, array.len()),
        Sampler { array } => (DescriptorType::SAMPLER, array.len()),
        Struct { array, .. } => (DescriptorType::UNIFORM_BUFFER, array.len()),
        Unknown | Void => (DescriptorType::UNIFORM_BUFFER, 1),
        Boolean { array, .. }
        | Char { array }
        | Int { array, .. }
        | UInt { array, .. }
        | Int64 { array, .. }
        | UInt64 { array, .. }
        | AtomicCounter { array }
        | Half { array, .. }
        | Float { array, .. }
        | Double { array, .. }
        | SByte { array, .. }
        | UByte { array, .. }
        | Short { array, .. }
        | UShort { array, .. } => (DescriptorType::UNIFORM_BUFFER, array.len()),
        _ => unimplemented!(),
    };
    res.1 = std::cmp::max(res.1, 1);
    res
}
