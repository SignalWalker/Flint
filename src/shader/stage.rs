use ash::vk::ShaderStageFlags;
use shaderc::*;
use std::cmp::Ordering;

/// Intermediary between various ShaderStage enums used by
/// other libraries.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum ShaderStage {
    Comp,
    Vert,
    Geom,
    Frag,
}

impl Ord for ShaderStage {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialOrd for ShaderStage {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        use ShaderStage::*;
        Some(match self {
            Comp => {
                if let Comp = other {
                    Ordering::Equal
                } else {
                    Ordering::Less
                }
            }
            Vert => match other {
                Comp => Ordering::Greater,
                Vert => Ordering::Equal,
                _ => Ordering::Less,
            },
            Frag => match other {
                Frag => Ordering::Equal,
                Geom => Ordering::Less,
                _ => Ordering::Greater,
            },
            Geom => {
                if let Geom = other {
                    Ordering::Equal
                } else {
                    Ordering::Greater
                }
            }
        })
    }
}

impl From<ShaderKind> for ShaderStage {
    fn from(k: ShaderKind) -> Self {
        match k {
            ShaderKind::Vertex => ShaderStage::Vert,
            ShaderKind::Geometry => ShaderStage::Geom,
            ShaderKind::Fragment => ShaderStage::Frag,
            _ => panic!("Not Implemented - ShaderKind -> ShaderStage"),
        }
    }
}

impl From<spirv_cross::spirv::ExecutionModel> for ShaderStage {
    fn from(s: spirv_cross::spirv::ExecutionModel) -> Self {
        use spirv_cross::spirv::ExecutionModel::*;
        use ShaderStage::*;
        match s {
            Vertex => Vert,
            Geometry => Geom,
            Fragment => Frag,
            _ => panic!("Not Implemented :: {:?} -> ShaderStage", s),
        }
    }
}

impl Into<ShaderKind> for ShaderStage {
    fn into(self) -> ShaderKind {
        match self {
            ShaderStage::Comp => ShaderKind::Compute,
            ShaderStage::Vert => ShaderKind::Vertex,
            ShaderStage::Geom => ShaderKind::Geometry,
            ShaderStage::Frag => ShaderKind::Fragment,
        }
    }
}

impl Into<ShaderStageFlags> for ShaderStage {
    fn into(self) -> ShaderStageFlags {
        match self {
            ShaderStage::Comp => ShaderStageFlags::COMPUTE,
            ShaderStage::Vert => ShaderStageFlags::VERTEX,
            ShaderStage::Geom => ShaderStageFlags::GEOMETRY,
            ShaderStage::Frag => ShaderStageFlags::FRAGMENT,
        }
    }
}

impl From<&str> for ShaderStage {
    fn from(s: &str) -> Self {
        match &s.to_lowercase()[..] {
            "comp" => ShaderStage::Comp,
            "vert" => ShaderStage::Vert,
            "frag" => ShaderStage::Frag,
            "geom" => ShaderStage::Geom,
            _ => panic!("Not Recognized: {}", s),
        }
    }
}
