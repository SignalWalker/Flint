use crate::shader::stage::ShaderStage;
use ash::{
    version::DeviceV1_0,
    vk::{PipelineShaderStageCreateInfo, ShaderModule},
    Device,
};

use std::ffi::CString;

pub struct ShaderArtifact {
    pub(super) dev: Device,
    pub stage: ShaderStage,
    pub entry: CString,
    pub module: ShaderModule,
}

impl Drop for ShaderArtifact {
    fn drop(&mut self) {
        eprintln!("Dropping shader artifact {:?}", self.module);
        unsafe { self.dev.destroy_shader_module(self.module, None) };
    }
}

impl ShaderArtifact {
    pub fn create_info(&self) -> PipelineShaderStageCreateInfo {
        PipelineShaderStageCreateInfo::builder()
            .module(self.module)
            .name(&self.entry)
            .stage(self.stage.into())
            .build()
    }
}
