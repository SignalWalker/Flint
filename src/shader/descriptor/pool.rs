use crate::buffer::*;
use crate::shader::*;
use ash::{
    version::DeviceV1_0,
    vk,
    vk::{DescriptorPool, DescriptorSetAllocateInfo, DescriptorSetLayoutBinding, ShaderStageFlags},
    Device,
};
use spirv_cross::{glsl, *};
use std::fmt::Debug;
use std::iter::FromIterator;

pub struct DescPoolToken {
    dev: Device,
    pub pool: DescriptorPool,
    pub sets: Vec<SetToken>,
    pub buffers: HashMap<String, Buffer>,
    stale_buffers: Vec<String>,
}

impl Debug for DescPoolToken {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("DescPoolToken")
            .field("pool", &self.pool)
            .field("sets", &self.sets)
            .finish()
    }
}

impl Drop for DescPoolToken {
    fn drop(&mut self) {
        eprintln!("Dropping descriptor pool {:?}", self.pool);
        unsafe {
            //self.pool.reset();
            self.dev.destroy_descriptor_pool(self.pool, None);
        }
    }
}

impl DescPoolToken {
    pub fn builder(min_offset: u64) -> DescPoolBuilder {
        DescPoolBuilder {
            min_offset,
            data: HashMap::new(),
            push_consts: HashMap::new(),
        }
    }

    pub fn init_buffers(&mut self, mem_prop: &vk::PhysicalDeviceMemoryProperties) {
        if !self.buffers.is_empty() {
            return;
        }
        self.buffers = HashMap::from_iter(
            self.sets
                .iter()
                .flat_map(|s| s.descriptors.iter())
                .filter_map(|(id, desc)| {
                    if desc.fields.is_empty() {
                        None
                    } else {
                        Some((id.clone(), desc.make_buffer(self.dev.clone(), mem_prop)))
                    }
                }),
        )
    }

    pub fn write_buffer<D: Copy>(&mut self, obj: &str, field: &str, data: &[D]) {
        if let Buffer::Struct(s) = &self.buffers[obj] {
            s.write(field, data);
            self.stale_buffers.push(obj.to_owned());
        } else {
            unimplemented!()
        }
    }

    pub fn first_set(&self) -> u32 {
        self.sets.iter().min_by(|a, b| a.id.cmp(&b.id)).unwrap().id
    }

    pub fn refresh_desc_sets(&mut self) {
        use crate::shader::descriptor::DescWriteInfo::*;
        self.update_desc_sets(
            self.stale_buffers
                .iter()
                .map(|obj| (obj.as_str(), Buf(self.buffers[obj].buf_info())))
                .collect::<Vec<_>>(),
        );
        self.stale_buffers.clear();
    }

    pub fn update_desc_sets(&self, mut info: Vec<(&str, DescWriteInfo)>) {
        let writes = self
            .sets
            .iter()
            .flat_map(|set| set.make_writes(&mut info))
            .collect::<Vec<_>>();
        //dbg!(&writes);
        unsafe {
            self.dev.update_descriptor_sets(&writes, &[]);
        }
    }
}

pub struct DescPoolBuilder {
    pub min_offset: vk::DeviceSize,
    pub data: HashMap<u32, Vec<Descriptor>>,
    pub push_consts: HashMap<String, PushConstant>,
}

impl DescPoolBuilder {
    pub fn add(&mut self, bin: &[u32]) -> &mut Self {
        let module = spirv::Module::from_words(bin);
        let ast = spirv::Ast::<glsl::Target>::parse(&module).unwrap();

        let stage: ShaderStageFlags = {
            let native: ShaderStage = ast.get_entry_points().unwrap()[0].execution_model.into();
            native.into()
        };
        let resources = ast.get_shader_resources().unwrap();

        for res in resources
            .uniform_buffers
            .iter()
            .chain(resources.sampled_images.iter())
            .chain(resources.separate_images.iter())
            .chain(resources.separate_samplers.iter())
            .chain(resources.storage_buffers.iter())
            .chain(resources.storage_images.iter())
        {
            let desc = Descriptor::new(&ast, stage, res, self.min_offset);
            self.data
                .entry(desc.set)
                .or_insert_with(Vec::new)
                .push(desc);
        }

        for push in resources.push_constant_buffers {
            let p = PushConstant::new(&ast, stage, &push, self.min_offset);
            self.push_consts.insert(push.name, p);
        }

        self
    }

    pub fn build(self, dev: Device) -> (DescPoolToken, HashMap<String, PushConstant>) {
        let pool = unsafe {
            let sizes = self
                .data
                .iter()
                .flat_map(|(_s, b)| b.iter())
                .map(Descriptor::size)
                .collect::<Vec<_>>();
            let info = vk::DescriptorPoolCreateInfo::builder()
                .pool_sizes(&sizes)
                .max_sets(self.data.len() as u32);
            dev.create_descriptor_pool(&info, None).unwrap()
        };

        // don't feel like explaining this; good luck (????-??-??)
        // wow, thanks (2019-09-23)
        let sets = unsafe {
            let layouts = self
                .data
                .into_iter()
                .map(|(_set_id, descs)| {
                    (
                        dev.create_descriptor_set_layout(
                            &vk::DescriptorSetLayoutCreateInfo::builder()
                                .bindings(
                                    &descs
                                        .iter()
                                        .cloned()
                                        .map(std::convert::Into::into)
                                        .collect::<Vec<DescriptorSetLayoutBinding>>(),
                                )
                                .build(),
                            None,
                        )
                        .unwrap(),
                        HashMap::from_iter(
                            descs.into_iter().map(|desc| (desc.name.to_string(), desc)),
                        ),
                    )
                })
                .collect::<Vec<_>>();

            dev.allocate_descriptor_sets(
                &DescriptorSetAllocateInfo::builder()
                    .descriptor_pool(pool)
                    .set_layouts(
                        &layouts
                            .iter()
                            .cloned()
                            .map(|(layout, _d)| layout)
                            .collect::<Vec<_>>()[..],
                    )
                    .build(),
            )
            .unwrap()
            .into_iter()
            .zip(layouts.into_iter())
            .map(|(set, (layout, descriptors))| SetToken {
                id: descriptors
                    .values()
                    .min_by(|a, b| a.set.cmp(&b.set))
                    .unwrap()
                    .set,
                dev: dev.clone(),
                set,
                layout,
                descriptors,
            })
            .collect::<Vec<_>>()
        };
        (
            DescPoolToken {
                dev,
                pool,
                sets,
                buffers: HashMap::new(),
                stale_buffers: Vec::new(),
            },
            self.push_consts,
        )
    }
}
