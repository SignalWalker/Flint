use crate::shader::*;
use ash::{
    version::DeviceV1_0,
    vk,
    vk::{DescriptorSet, DescriptorSetLayout},
    Device,
};

use std::fmt::Debug;

pub struct SetToken {
    pub(super) dev: Device,
    pub set: DescriptorSet,
    pub layout: DescriptorSetLayout,
    pub descriptors: HashMap<String, Descriptor>,
    pub id: u32,
}

impl Debug for SetToken {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("SetToken")
            .field("set", &self.set)
            .field("layout", &self.layout)
            .field("descriptors", &self.descriptors)
            .finish()
    }
}

impl Drop for SetToken {
    fn drop(&mut self) {
        eprintln!("Dropping set token layout {:?}", self.layout);
        unsafe {
            self.dev.destroy_descriptor_set_layout(self.layout, None);
        }
    }
}

impl SetToken {
    pub fn make_writes(&self, info: &mut [(&str, DescWriteInfo)]) -> Vec<vk::WriteDescriptorSet> {
        info.iter_mut()
            .filter_map(|(name, info)| {
                if !self.descriptors.contains_key(*name) {
                    None
                } else {
                    Some(self.descriptors[*name].make_write(self.set, info))
                }
            })
            .collect::<Vec<_>>()
    }
}
