use crate::shader::*;
use ash::{version::DeviceV1_0, vk, vk::ShaderStageFlags, Device};
use spirv_cross::*;
use std::fmt::Debug;

#[derive(Debug)]
pub struct PushConstant {
    pub range: vk::PushConstantRange,
    pub fields: HashMap<String, DescField>,
}

impl PushConstant {
    pub fn write<D>(
        &self,
        dev: &Device,
        cmd_buf: vk::CommandBuffer,
        layout: vk::PipelineLayout,
        field: &str,
        data: &[D],
    ) {
        let desc = &self.fields[field];
        let data = unsafe {
            std::slice::from_raw_parts(data.as_ptr() as *const u8, std::mem::size_of_val(data))
        };
        {
            let data_size = std::mem::size_of_val(data);
            if data_size != desc.size as usize {
                panic!(
                    "PushConstant::write() called with size(data) != size({}): {} != {}",
                    field, data_size, desc.size
                );
            }
        }
        unsafe {
            dev.cmd_push_constants(
                cmd_buf,
                layout,
                self.range.stage_flags,
                self.range.offset + desc.offset as u32,
                data,
            )
        }
    }

    pub(super) fn new<T>(
        ast: &spirv::Ast<T>,
        stage: ShaderStageFlags,
        res: &spirv::Resource,
        min_offset: vk::DeviceSize,
    ) -> Self
    where
        T: spirv::Target,
        spirv::Ast<T>: spirv::Compile<T> + spirv::Parse<T>,
    {
        use std::convert::TryInto;
        let ty = ast.get_type(res.type_id).unwrap();
        let fields = DescField::from_desc_res(&ast, &ty, &res, min_offset);
        Self {
            range: vk::PushConstantRange {
                stage_flags: stage,
                size: {
                    let last = fields
                        .iter()
                        .max_by(|a, b| a.1.offset.cmp(&b.1.offset))
                        .unwrap();
                    (last.1.offset + last.1.size).try_into().unwrap()
                },
                offset: 0, // TODO :: Demagic this
            },
            fields,
        }
    }
}
