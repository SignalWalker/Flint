use crate::base::swapchain::SwapToken;
use crate::base::VkContext;
use crate::shader::*;

use crate::vertex::*;
use ash::vk::RenderPass;
use ash::{
    version::{DeviceV1_0, InstanceV1_0},
    vk,
    vk::{
        CommandBuffer, GraphicsPipelineCreateInfo, Pipeline, PipelineBindPoint, PipelineLayout,
        PipelineLayoutCreateInfo, PipelineViewportStateCreateInfo, Rect2D, Viewport,
    },
    Device,
};
use std::collections::HashMap;
use std::fmt::Debug;

pub fn gen_material<S: ToString>(
    vk: &VkContext,
    swapchain: &mut SwapToken,
    name: S,
    paths: Vec<(&str, &str)>,
    polygon_mode: vk::PolygonMode,
) -> HashMap<String, PushConstant> {
    let (shaders, desc_pool, push_consts) = {
        let mut compiler = shaderc::Compiler::new().unwrap();
        let dev_prop = unsafe { vk.instance.get_physical_device_properties(vk.pdevice) };
        let mut min_offset = dev_prop.limits.min_uniform_buffer_offset_alignment;
        let max_size = dev_prop.limits.max_push_constants_size;
        // Card on windows reports min offset as 256, which is also max push const size,
        // so I'm guessing it means there's no min, because this works.
        if min_offset == u64::from(max_size) {
            min_offset = 1;
        }
        MetaShader::build_chain(
            vk.device.clone(),
            MetaShader::new_chain(&mut compiler, paths, None),
            min_offset,
        )
    };

    swapchain.make_pipeline(
        name,
        desc_pool,
        shaders,
        push_consts.values().collect::<Vec<_>>(),
        polygon_mode,
    );
    push_consts
}

pub struct PipeBuilder {
    shader_paths: Vec<(String, String)>,
}

impl PipeBuilder {
    pub fn with_shader(&mut self, path: String, main: String) -> &mut Self {
        self.shader_paths.push((path, main));
        self
    }

    pub fn build(self) -> PipeToken {
        unimplemented!()
    }
}

struct PipeRecreationData {
    pub(crate) shader_info: Vec<vk::PipelineShaderStageCreateInfo>,
    pub(crate) polygon_mode: vk::PolygonMode,
}

unsafe impl Send for PipeRecreationData {}
unsafe impl Sync for PipeRecreationData {}

pub struct PipeToken {
    dev: Device,
    pub desc_pool: DescPoolToken,
    pub pipe: Pipeline,
    pub layout: PipelineLayout,
    pub bind_point: PipelineBindPoint,
    //pub push_consts: HashMap<String, PushConstant>,
    #[allow(dead_code)] // this exists just to keep artifacts from being dropped
    shaders: Vec<ShaderArtifact>,
    recreation_data: PipeRecreationData,
}

impl Debug for PipeToken {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("PipeToken")
            .field("desc_pool", &self.desc_pool)
            .field("pipe", &self.pipe)
            .field("layout", &self.layout)
            .finish()
    }
}

impl Drop for PipeToken {
    fn drop(&mut self) {
        eprintln!("Dropping Pipetoken");
        unsafe {
            self.dev.destroy_pipeline(self.pipe, None);
            self.dev.destroy_pipeline_layout(self.layout, None);
        }
    }
}

impl PipeToken {
    pub fn bind_sets(&self, cmd_buf: CommandBuffer) {
        unsafe {
            self.dev.cmd_bind_descriptor_sets(
                cmd_buf,
                self.bind_point,
                self.layout,
                self.desc_pool.first_set(),
                &self
                    .desc_pool
                    .sets
                    .iter()
                    .map(|s| s.set)
                    .collect::<Vec<_>>(),
                &[],
            );
        }
    }

    pub fn bind(&self, buf: CommandBuffer) {
        self.bind_sets(buf);
        unsafe { self.dev.cmd_bind_pipeline(buf, self.bind_point, self.pipe) };
    }

    pub fn recreate(&mut self, renderpass: RenderPass, viewport: Viewport, scissors: Rect2D) {
        unsafe {
            self.dev.destroy_pipeline(self.pipe, None);
        }
        self.pipe = Self::make_pipeline(
            &self.dev,
            viewport,
            scissors,
            &self.recreation_data.shader_info,
            self.layout,
            renderpass,
            self.recreation_data.polygon_mode,
        );
    }

    pub fn build(
        dev: Device,
        pool: DescPoolToken,
        push_consts: Vec<&PushConstant>,
        renderpass: RenderPass,
        viewport: Viewport,
        scissors: Rect2D,
        shaders: HashMap<ShaderStage, ShaderArtifact>,
        polygon_mode: vk::PolygonMode,
    ) -> Self {
        let layout = {
            let layouts = pool.sets.iter().map(|desc| desc.layout).collect::<Vec<_>>();
            let p_consts = push_consts
                .iter()
                .map(|push| push.range)
                .collect::<Vec<_>>();
            let info = PipelineLayoutCreateInfo::builder()
                .set_layouts(&layouts)
                .push_constant_ranges(&p_consts);
            unsafe { dev.create_pipeline_layout(&info, None) }.unwrap()
        };

        let shader_info = {
            // TODO - Find out whether sorting this is necessary
            let mut sort = shaders.iter().collect::<Vec<_>>();
            sort.sort_by(|(s, _a), (os, _oa)| s.cmp(os));
            sort.into_iter()
                .map(|(_s, a)| a.create_info())
                .collect::<Vec<_>>()
        };

        let pipe = Self::make_pipeline(
            &dev,
            viewport,
            scissors,
            &shader_info,
            layout,
            renderpass,
            polygon_mode,
        );

        PipeToken {
            dev,
            desc_pool: pool,
            pipe,
            layout,
            shaders: shaders.into_iter().map(|(_stage, shd)| shd).collect(),
            bind_point: PipelineBindPoint::GRAPHICS,
            recreation_data: PipeRecreationData {
                shader_info,
                polygon_mode,
            },
        }
    }

    fn make_pipeline(
        dev: &Device,
        viewport: Viewport,
        scissors: Rect2D,
        shader_info: &[PipelineShaderStageCreateInfo],
        layout: PipelineLayout,
        renderpass: RenderPass,
        polygon_mode: vk::PolygonMode,
    ) -> Pipeline {
        let (v_attr, v_bind) = (Vertex::attr_descs(), Vertex::bind_descs());
        let v_input_state = Vertex::input_state_info(&v_attr, &v_bind);
        let v_asm_state = Vertex::asm_state_info();
        let viewport_state = PipelineViewportStateCreateInfo::builder()
            .scissors(&[scissors])
            .viewports(&[viewport])
            .build();
        let raster_state = vk::PipelineRasterizationStateCreateInfo {
            front_face: vk::FrontFace::CLOCKWISE,
            line_width: 1.0,
            polygon_mode,
            cull_mode: vk::CullModeFlags::BACK,
            ..Default::default()
        };
        let multisample_state = vk::PipelineMultisampleStateCreateInfo::builder()
            .rasterization_samples(vk::SampleCountFlags::TYPE_1)
            .build();
        let noop_depth = vk::StencilOpState {
            fail_op: vk::StencilOp::KEEP,
            pass_op: vk::StencilOp::KEEP,
            depth_fail_op: vk::StencilOp::KEEP,
            compare_op: vk::CompareOp::ALWAYS,
            ..Default::default()
        };
        let depth_state = vk::PipelineDepthStencilStateCreateInfo {
            depth_test_enable: 1,
            depth_write_enable: 1,
            depth_compare_op: vk::CompareOp::LESS_OR_EQUAL,
            front: noop_depth,
            back: noop_depth,
            max_depth_bounds: 1.0,
            ..Default::default()
        };
        let color_blend_attachment_states = [vk::PipelineColorBlendAttachmentState {
            blend_enable: 0,
            src_color_blend_factor: vk::BlendFactor::SRC_COLOR,
            dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_DST_COLOR,
            color_blend_op: vk::BlendOp::ADD,
            src_alpha_blend_factor: vk::BlendFactor::ZERO,
            dst_alpha_blend_factor: vk::BlendFactor::ZERO,
            alpha_blend_op: vk::BlendOp::ADD,
            color_write_mask: vk::ColorComponentFlags::all(),
        }];
        let blend_state = vk::PipelineColorBlendStateCreateInfo::builder()
            .logic_op(vk::LogicOp::CLEAR)
            .attachments(&color_blend_attachment_states)
            .build();
        let dyn_state_arr = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dyn_state = vk::PipelineDynamicStateCreateInfo::builder()
            .dynamic_states(&dyn_state_arr)
            .build();

        let pipe_info = GraphicsPipelineCreateInfo::builder()
            .stages(&shader_info)
            .vertex_input_state(&v_input_state)
            .input_assembly_state(&v_asm_state)
            .viewport_state(&viewport_state)
            .rasterization_state(&raster_state)
            .multisample_state(&multisample_state)
            .depth_stencil_state(&depth_state)
            .color_blend_state(&blend_state)
            .dynamic_state(&dyn_state)
            .layout(layout)
            .render_pass(renderpass)
            .build();

        unsafe {
            dev.create_graphics_pipelines(vk::PipelineCache::null(), &[pipe_info], None)
                .unwrap()
                .remove(0)
        }
    }
}
